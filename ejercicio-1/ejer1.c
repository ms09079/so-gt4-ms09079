#include <stdio.h>
float main(){
float n1;
float n2;
float *p1;/*Variable de tipo puntero*/
float *p2;/*Variable de tipo puntero*/

n1=4.3;
//printf("Valor de n1=%3.2f\r\n", &n1);
p1=&n1; /*asigna a p1 la direccion de x*/
//printf("Valor de p1=%d\n", *p1);
p2=p1;/*asigna a p2 la direccion de p1*/
//printf("Valor de p2=%d\n", *p2);
n2=*p2; /*asigna a n2 la direccion de p2*/
printf("Valor de n2=%3.2f\r\n", &n2);
n1=*p1+*p2;
printf("Al sumar los punteros el valor de n1=%3.2f\r\n", &n1);


}
